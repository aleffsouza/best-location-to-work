module.exports = {
	...require('gts/.prettierrc.json'),
	singleQuote: false,
	useTabs: true,
	tabWidth: 2,
};
