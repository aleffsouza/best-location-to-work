import * as React from "react";

type TemperatureProps = {
	value: number;
	className?: string;
};

const Temperature = ({value, className}: TemperatureProps) => {
	return <h4 className={className}>{value}°C</h4>;
};

export default Temperature;
