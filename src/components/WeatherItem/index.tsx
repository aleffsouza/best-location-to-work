import * as React from "react";
import Temperature from "../Temperature";
import {WeatherItemWrapper} from "./WeatherItem.styles";
import SunnyIcon from "../../../assets/imgs/icons/sunny.svg";
import CloudyIcon from "../../../assets/imgs/icons/cloudy.svg";
import RainIcon from "../../../assets/imgs/icons/rain.svg";

type WeatherItemProps = {
	icon: string;
	temperature: number;
	weatherCondition: WeatherCondition;
};

enum WeatherCondition {
	Sunny = "sunny",
	Cloudy = "cloudy",
	Rain = "rain",
}

function getWeatherConditionIcon(weatherCondition: WeatherCondition) {
	let result = undefined;

	if (weatherCondition === WeatherCondition.Sunny) {
		result = SunnyIcon;
	} else if (weatherCondition === WeatherCondition.Cloudy) {
		result = CloudyIcon;
	} else if (weatherCondition === WeatherCondition.Rain) {
		result = RainIcon;
	}

	return result;
}

const WeatherItem = ({
	icon,
	temperature,
	weatherCondition,
}: WeatherItemProps) => {
	let weatherIcon = icon;

	const result = getWeatherConditionIcon(weatherCondition);

	if (result) {
		weatherIcon = result;
	}

	return (
		<WeatherItemWrapper>
			<img src={weatherIcon} />
			<Temperature value={temperature} />
		</WeatherItemWrapper>
	);
};

export default WeatherItem;
