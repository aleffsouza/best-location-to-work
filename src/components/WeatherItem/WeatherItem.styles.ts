import styled from "styled-components";

export const WeatherItemWrapper = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	text-align: center;
	align-items: center;
	justify-content: space-between;

	img {
		margin-top: 0.5em;
	}

	h4 {
		margin-top: 0.5em;
	}

	@media screen and (max-width: 1080px) {
		h4 {
			display: none;
		}

		img {
			margin-top: 0;
		}
	}
`;
