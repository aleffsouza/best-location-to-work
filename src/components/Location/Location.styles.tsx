import styled from "styled-components";

export const LocationWrapper = styled.div`
	display: flex;
	align-items: center;

	@media screen and (max-width: 600px) {
		flex-direction: column;
	}
`;

export const LocationPoster = styled("div")<{imgSrc: string}>`
	border-radius: 10%;
	background: url(${props => props.imgSrc});
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	height: 300px;
	width: 350px;

	@media screen and (max-width: 600px) {
		width: 100%;
		height: 300px;
		border-radius: unset;
	}

	@media screen and (max-width: 1080px) {
		height: 400px;
		width: 500px;
	}
`;

export const DetailsWrapper = styled.div`
	margin-left: 2em;
	width: 100%;
	height: 100%;

	@media screen and (max-width: 1080px) {
		margin-left: 0;
		padding: 0 1.2em;

		.mob-location-title {
			margin-top: 1em;
		}
	}
`;
