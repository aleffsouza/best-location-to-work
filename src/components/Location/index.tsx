import * as React from "react";
import WeekForecast from "../WeekForecast";
import {
	LocationWrapper,
	DetailsWrapper,
	LocationPoster,
} from "./Location.styles";

type LocationProps = {
	name: string;
	imageUrl: string;
	weekForecast: any[];
};

const Location = ({name, imageUrl, weekForecast}: LocationProps) => {
	return (
		<LocationWrapper>
			<LocationPoster imgSrc={imageUrl} />

			<DetailsWrapper>
				<h2 className="mob-location-title">{name}</h2>
				<WeekForecast values={weekForecast} />
			</DetailsWrapper>
		</LocationWrapper>
	);
};

export default Location;
