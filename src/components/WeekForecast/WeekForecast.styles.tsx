import styled from 'styled-components';

type DayForecastItemWrapperProps = {
	bestOption?: boolean;
	cheapest?: boolean;
};

export const DayForecastItemWrapper = styled('div')<
	DayForecastItemWrapperProps
>`
	padding: 2em;

	${props =>
		props.bestOption
			? 'background: #c4ffd4; border: 2px #34a853 solid;'
			: undefined};

	@media screen and (max-width: 1080px) {
		padding: 0;
	}
`;

export const DayForecastItem = styled('div')<{bestOption?: boolean}>`
	display: grid;
	grid-template-rows: 1em auto;
	flex-direction: column;
	justify-items: center;
	align-items: center;

	div > img {
		width: 60px;
		height: 60px;
	}

	.best-option {
		grid-row-start: 1;
		text-align: center;
		margin-bottom: 1em;
	}

	@media screen and (max-width: 1080px) {
		grid-template-columns: 1fr 1fr 1fr 1fr;

		div > img {
			width: 30px;
			height: 30px;
		}

		.best-option {
			display: none;
		}
	}

	@media screen and (max-width: 1080px) {
		padding: 1em;
	}
`;

export const WeekForecastWrapper = styled.div`
	display: flex;
	margin-top: 2em;
	padding: 0 2.5em;
	justify-content: space-between;

	@media screen and (max-width: 1080px) {
		flex-direction: column;
		padding: unset;
		margin-top: 0;
	}
`;
