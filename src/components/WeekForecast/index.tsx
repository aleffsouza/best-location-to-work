import * as React from "react";
import WeatherItem from "../WeatherItem";
import Temperature from "../Temperature";
import {
	WeekForecastWrapper,
	DayForecastItemWrapper,
	DayForecastItem,
} from "./WeekForecast.styles";

type WeekForecastProps = {
	values: any[];
};

const WeekForecast = ({values}: WeekForecastProps) => {
	return (
		<WeekForecastWrapper>
			{values.map((v, i) => {
				return (
					<DayForecastItemWrapper key={i} {...v}>
						<DayForecastItem key={i}>
							<div className="best-option">
								{v.bestOption ? <h3>&#128081;</h3> : null}
							</div>

							<h3>{v.date}</h3>

							<WeatherItem {...v} />

							<Temperature value={v.temperature} className="up-to-hd" />

							<h4>${v.price}</h4>
						</DayForecastItem>
					</DayForecastItemWrapper>
				);
			})}
		</WeekForecastWrapper>
	);
};

export default WeekForecast;
