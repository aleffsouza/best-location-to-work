import styled from "styled-components";

export const AppWrapper = styled.div`
	width: 100%;
	height: 100%;
	margin: 4em 0 4em 0;
	display: grid;
	grid-template-columns: 10% auto 10%;

	.content {
		grid-column-start: 2;
	}

	.locations-grid {
		display: grid;
		grid-row-gap: 4em;
	}

	@media screen and (max-width: 600px) {
		grid-template-columns: auto;
	}
`;

export const AppHeaderWrapper = styled.div`
	margin-bottom: 5em;

	@media screen and (max-width: 600px) {
		margin-bottom: 3em;
		padding: 0 1.2em;
	}
`;
