import React, {useEffect, useState} from "react";
import Location from "../../components/Location";
import GlobalStyle from "../../GlobalStyle";
import {AppWrapper, AppHeaderWrapper} from "./App.styles";
import * as ApiService from "../../services/api";
import Loading from "../../components/Loading";

const App = () => {
	const [data, setData] = useState<any>();
	const [loading, setLoading] = useState<boolean>(true);

	useEffect(() => {
		setLoading(true);
		ApiService.fetchData().then(data => {
			setData(data);
			setLoading(false);
		});
	}, []);

	return (
		<>
			<GlobalStyle />

			<AppWrapper>
				<div className="content">
					{loading ? (
						<Loading />
					) : (
						<>
							<AppHeaderWrapper>
								<h1>Welcome, {data.user.name} &#128075;&#127997;</h1>
								<br />
								<h4>
									Let&#39;s find out what&#39;s the best office location to work
									at this week!
								</h4>
							</AppHeaderWrapper>
							<div className="locations-grid">
								{data.locations.map(
									({city, country, poster, weekForecast}, index) => {
										const location = `${city}, ${country}`;
										return (
											<Location
												key={`${city}-${country}-${index}`}
												name={location}
												imageUrl={poster}
												weekForecast={weekForecast}
											/>
										);
									}
								)}
							</div>
						</>
					)}
				</div>
			</AppWrapper>
		</>
	);
};

export default App;
