import {createGlobalStyle} from "styled-components";

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-family: "Montserrat", sans-serif;
    word-break: normal;
    max-width: 100%;
  }

	h1 {
		font-size: 1.8em;
	}

	h2 {
		font-size: 1.6em;
	}

	h3 {
		font-size: 1.4em;
	}

	h4 {
		font-size: 1.2em;
	}

  p {
    font-size: 14px;
  }

	.up-to-hd {
		display: none;
	}

	@media screen and (max-width: 1080px) {
		h1 {
			font-size: 1.6em;
		}

		h2 {
			font-size: 1.4em;
		}

		h3 {
			font-size: 1.2em;
		}

		h4 {
			font-size: 1em;
		}

		p {
			font-size: 14px;
		}


		.up-to-hd {
			display: block;
		}
	}
`;

export default GlobalStyle;
