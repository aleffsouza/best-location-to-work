const data = [
	{
		city: "Madrid",
		country: "Spain",
		poster:
			"https://www.thetimes.co.uk/expert-traveller/wp-content/uploads/2018/11/GettyImages-951917136-2200x880.jpg",
		weekForecast: [
			{
				date: "14/10",
				weatherCondition: "sunny",
				temperature: 18,
				price: "200",
			},
			{
				date: "15/10",
				weatherCondition: "sunny",
				temperature: 18,
				bestOption: true,
				price: "80",
			},
			{
				date: "16/10",
				weatherCondition: "sunny",
				temperature: 17,
				price: "150",
			},
			{
				date: "17/10",
				weatherCondition: "sunny",
				temperature: 19,
				price: "120",
			},
			{
				date: "18/10",
				weatherCondition: "sunny",
				temperature: 21,
				price: "130",
			},
			{
				date: "19/10",
				weatherCondition: "sunny",
				temperature: 23,
				price: "100",
			},
			{
				date: "20/10",
				weatherCondition: "rain",
				temperature: 23,
				price: "90",
			},
		],
	},
	{
		city: "Amsterdam",
		country: "Netherlands",
		poster:
			"https://i.guim.co.uk/img/media/f62f9ed605add7b0651115e17de2b0242c6a8772/0_204_5472_3285/master/5472.jpg?width=1200&height=900&quality=85&auto=format&fit=crop&s=22e879f7a7c66c56451328f2daa15333",
		weekForecast: [
			{
				date: "14/10",
				weatherCondition: "cloudy",
				temperature: 11,
				price: "120",
			},
			{
				date: "15/10",
				weatherCondition: "cloudy",
				temperature: 11,
				price: "180",
			},
			{
				date: "16/10",
				weatherCondition: "cloudy",
				temperature: 12,
				price: "150",
			},
			{
				date: "17/10",
				weatherCondition: "sunny",
				temperature: 13,
				bestOption: true,
				price: "95",
			},
			{
				date: "18/10",
				weatherCondition: "cloudy",
				temperature: 13,
				price: "110",
			},
			{
				date: "19/10",
				weatherCondition: "cloudy",
				temperature: 13,
				price: "130",
			},
			{
				date: "20/10",
				weatherCondition: "rain",
				temperature: 13,
				price: "200",
			},
		],
	},
	{
		city: "Budapest",
		country: "Hungary",
		poster:
			"https://d12dkjq56sjcos.cloudfront.net/pub/media/wysiwyg/budapest/01-city-landing/BBT-Budapest-City-Landing-Night-View.jpg",
		weekForecast: [
			{
				date: "14/10",
				weatherCondition: "rain",
				temperature: 8,
				price: "200",
			},
			{
				date: "15/10",
				weatherCondition: "rain",
				temperature: 12,
				price: "220",
			},
			{
				date: "16/10",
				weatherCondition: "rain",
				temperature: 12,
				price: "200",
			},
			{
				date: "17/10",
				weatherCondition: "rain",
				temperature: 10,
				price: "252",
			},
			{
				date: "18/10",
				weatherCondition: "cloudy",
				temperature: 10,
				price: "300",
			},
			{
				date: "19/10",
				weatherCondition: "sunny",
				temperature: 12,
				bestOption: true,
				price: "190",
			},
			{
				date: "20/10",
				weatherCondition: "cloudy",
				temperature: 12,
				price: "300",
			},
		],
	},
];

const delay = async (ms: number) => {
	return new Promise((resolve, _) => setTimeout(resolve, ms));
};

export async function fetchData() {
	await delay(500);
	return {
		user: {
			name: "Jamie",
		},
		locations: data,
	};
}
