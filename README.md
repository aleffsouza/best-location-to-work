Author: Aleff Souza <souza.aleff@gmail.com>

# The Problem

Jamie's vacation is about to end. She works at an international company, so she can choose another office to work from: either Amsterdam, Madrid, or Budapest. Help her choose which office to go to – she’d like someplace with good weather or cheap flights (or both).

If you'd like to use an API (it's optional), you could try the AccuWeather API ( https://developer.accuweather.com ), the Kiwi API ( https://docs.kiwi.com ), or another of your choosing.

You have full control over how you want to present your data, but keep in mind this is a client-side application and Jamie needs to have enough information to make her choice. Make sure to publish your result on GitHub and provide an online demo. You can pick a framework, and the browser compatibility is up to you. We really value creativity, and we encourage you to create your own requirements while working on this assignment. It would be great if you could finish this within one week.

We recommend that you do not spend more than 4 hours on the assignment, and it is obviously not possible to implement all aspects of a production-ready application in this timeframe. This is why we suggest to focus on delivering a functional solution, while showcasing your strengths within the time indication.

Some of example focus areas are:

- Technical excellence
- Jaw-dropping UX
- Extremely secure app
- Framework guru (Vue, React or other)

We also encourage you to document your choices and possible improvement areas, in order to give us an insight into your line of thought, and help us evaluate your assignment accordingly.

# Introduction

The user needs a broader view from at least one week for planning what is the best location to work at.

So I was looking into ways to show off the minimum amount of information necessary for users to make the best decision, despite the "best option" has a green background it's up to the user to decide what the real "best option" is.

So I made some assumptions:

- Jamie's based in London (so we can assume the flight prices are related to her location, despite the data is set as )
- Temperature is measured in Celsius.
- I mocked the API to return all locations in order by the best location to work at, so the first item returned by that API means that it's the highlight that Jamie should look at first.

### The UX/UI Design Process

After some research, I realized the best way to show off the data would be using a calendar-like view because users will feel more familiar with such a way to present it.  

I was also willing to make an application that would be completely responsive, adapting the view for different screen resolutions.  

# Architecture

**Tech stack:** Typescript, React, styled-components, gts, yarn, parcel, eslint, prettier
This app uses Typescript and React.js, I chose to use Typescript for making the refactoring process easier as I started prototyping the application with dummy data inside the components, defining types for each component props helped me out to connect the docs when I decided to use the API Service module to fetch the data in a different format.
Despite `styled-components` has some performance drawbacks I used it because of speed for coverting cross-browser CSS.
Parcel is used to build the application.

### API Service
I would advocate hiding the details of the implementations from the client for fetching weather and flight prices because based on the user behavior the backend could make better recommendations and predictions (maybe with Machine Learning). Another reason for that would be to not expose any credentials on the client-side application that is not a good idea.

### Code Structure
```
./                              # root
├── src/
│   ├── components/             # Stateless UI components
│   ├── containers/             #
│   │   └── App/                # Wraps page content and passes
│   ├── index.ts                # Main app file
│   └── index.html
├── tsconfig.json               # Typescript config file
└── package.json                # Project info, scrips, deps, etc...
```

# Development
Notice that it's required npm version >= 5.2.0 to run this app, otherwise We recommend you to remove all npx entries from the script property on package.json and install Parcel as a global dependency (npm i -g parcel-bundler).

Run the steps bellow in the project root directory.
`yarn`
`yarn build:dev`
The app will be up and running on http://localhost:1234

# Deployment
Running the command `yarn build` will generate a `dist` folder in the root of this project directory.

The app is deployed on render.com:
https://adyen-take-home-assignment.onrender.com/

# Improvements
- Add tests with Jest
- Add filters for temperature and price range
- Display min/max temperature
- Let the user choose temperature in C or F
- Improve Loading state visually (better design for the loading component as well as some animations when the data comes-in)
- Let the user choose skin tone for emojis so the application is more
- Display forecast for Week/Month
- Support different currencies
- Support i18n
- Use theme for better organize the measurements and colors used.
- Dark mode